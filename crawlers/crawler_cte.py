import subprocess
import requests
from lxml import html
import base64
import tempfile


class Crawler_CTE(object):
    def __init__(self):
        # inicia a sessão
        self.session = requests.Session()

    def run(self, chave_cte):
        # primeiro request buscando a sessão, a imagem do captcha e os parametros do input
        url = "http://www.cte.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=completa&tipoConteudo=mCK/KoCqru0="
        response = self.session.get(url)
        page = html.fromstring(response.text)

        # xpath buscando todas as variaveis para o input
        image_base64 = page.xpath('//img[@id="ctl00_ContentPlaceHolder1_imgCaptcha"]/@src')[0].split(',')[1]
        view_state = page.xpath('//input[@id="__VIEWSTATE"]/@value')[0]
        view_state_generator = page.xpath('//input[@id="__VIEWSTATEGENERATOR"]/@value')[0]
        event_validation = page.xpath('//input[@id="__EVENTVALIDATION"]/@value')[0]
        token = page.xpath('//input[@id="ctl00_ContentPlaceHolder1_token"]/@value')[0]
        captcha_som = page.xpath('//input[@id="ctl00_ContentPlaceHolder1_captchaSom"]/@value')[0]

        # cria um arquivo temporario .png decodificando o base64 do captcha
        with tempfile.NamedTemporaryFile(suffix='.png') as temp_png:
            file = open(temp_png.name, 'wb')
            file.write(base64.b64decode(image_base64))
            file.close()

            # executa um subprocess do meu script em R para quebrar o captcha e retornar a string
            r = subprocess.check_output(['Rscript', 'quebra_captcha.R', temp_png.name])
            captcha = r.decode('utf-8').replace('[1] "', "").replace('"', "").replace('\n', '')

        # setar os parametros para o POST
        cookies = {
            'ASP.NET_SessionId': self.session.cookies.get('ASP.NET_SessionId'),
        }

        params = (
            ('tipoConsulta', 'completa'),
            ('tipoConteudo', 'mCK/KoCqru0='),
        )
        data = {
            '__EVENTTARGET': '',
            '__EVENTARGUMENT': '',
            '__VIEWSTATE': view_state,
            '__VIEWSTATEGENERATOR': view_state_generator,
            '__EVENTVALIDATION': event_validation,
            'ctl00$txtPalavraChave': '',
            'ctl00$ContentPlaceHolder1$txtChaveAcessoCompleta': chave_cte,
            'ctl00$ContentPlaceHolder1$txtCaptcha': captcha,
            'ctl00$ContentPlaceHolder1$btnConsultar': 'Continuar',
            'ctl00$ContentPlaceHolder1$token': token,
            'ctl00$ContentPlaceHolder1$captchaSom': captcha_som,
            'hiddenInputToUpdateATBuffer_CommonToolkitScripts': '1'
        }

        # segundo request POST para entrar na consulta
        response = self.session.post('http://www.cte.fazenda.gov.br/portal/consulta.aspx',
                                     params=params,
                                     cookies=cookies,
                                     data=data)

        # verifica se o captcha foi resolvido com sucesso, caso não tenha sido, ele executa novamente o método
        if "Código da Imagem inválido. Tente novamente." in response:
            return self.run(chave_cte)

        page = html.fromstring(response.text)

        # alguns xpaths para pegar as informações da consulta
        razao_emitente = page.xpath('//th[contains(text(), "Nome / Razão Social")]/following::td/text()')[1]
        cnpj_emitente = page.xpath('//th[contains(text(), "Nome / Razão Social")]/following::td/text()')[0]
        cabecalho = page.xpath('//*[@id="tabs"]/table//td/text()')

        data_emissao = page.xpath('//div[contains(text(), "Dados do CT-e")]/following::table[1]//td[3]/text()')[0]
        data_emissao = data_emissao.replace("\r\n       ", "")

        valor_total = page.xpath('//th[contains(text(), "Valor Total Serviço")]/following::td[1]/text()')[0]
        base_calculo_icms = page.xpath('//th[contains(text(), "Valor Total Serviço")]/following::td[2]/text()')[0]
        valor_icms = page.xpath('//th[contains(text(), "Valor Total Serviço")]/following::td[3]/text()')[0]

        # monta um dicionário de resposta
        resposta = {
            "razao_emitente": razao_emitente,
            "cnpj_emitente": cnpj_emitente,
            "chave_cte": cabecalho[0],
            "numero_cte": cabecalho[1],
            "serie": cabecalho[2],
            "versao_xml": cabecalho[3],
            "data_emissao": data_emissao,
            "valor_total": valor_total,
            "base_calculo_icms": base_calculo_icms,
            "valor_icms": valor_icms,
        }

        return resposta

