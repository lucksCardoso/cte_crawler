from flask import Flask, jsonify
from crawlers.crawler_cte import Crawler_CTE
app = Flask(__name__)


@app.route('/')
def index():
    return jsonify({"Message": "bem vindo a API CT-e"})


@app.route('/cte/<c>', methods=["GET"])
def buscar_cte(c):
    cte = Crawler_CTE()
    resposta = cte.run(c)
    return jsonify(resposta)


if __name__ == "__main__":
    app.run(debug=False, host="0.0.0.0")
