# Crawler CT-e API

## Sobre
Este projeto foi desenvolvido com as liguangens  R e Python.

Com o R eu escrevi um simples script para quebrar o Captcha,
com a lib [decryptr](https://github.com/decryptr/decryptr "decryptr"), onde consegui mais de 95% de sucesso.

Com o Python utilizei a lib [Requests](http://docs.python-requests.org/en/master/ "Requests") para fazer as requisições e captura de dados.

## Instalação (Ubuntu)
###Instalar o R:
`$ sudo apt install r-base`

###Instalar os packeges no terminal do R:
`> install.packages('devtools')`

`> install.packages("reticulate")`

`> install.packages("keras")`

`> library(keras)`

`> install_keras()`

`> devtools::install_github('decryptr/decryptr')` -> (ESSE DEMORA UM POUCO PRA INSTALAR)
###Criar uma virtualenv:
`$ virtualenv venv -p python3`
###Instalar os requisitos do python:
`$ pip install -r requirements`
###Rode a API:
`$ python app.py`

##Endpoint:
/cte/chave_cte

retorna um Json com algumas informações.